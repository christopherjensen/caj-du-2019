package com.daugherty;

import java.util.ArrayList;

public interface IInterestCalculator {
    public void addRule(IInterestRule rule);
    public double claculateInterest(int yearOfCreation, double balance);
}