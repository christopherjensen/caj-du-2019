public class GlassOverflownException extends Exception {
    public GlassOverflownException(String message) {
        super(message);
    }

    public GlassOverflownException(String message, Throwable cause) {
        super(message + ": ", cause);
    }

}
