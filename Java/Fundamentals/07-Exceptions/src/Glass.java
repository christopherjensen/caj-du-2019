public class Glass {

    private float maxGallons;
    private float currentGallons;

    public Glass(float maxGallons) {
        this.maxGallons = maxGallons;
    }

    public void addWater(float gallonsToAdd) throws GlassOverflownException {
        float waterLevel = this.currentGallons + gallonsToAdd;
        if (waterLevel > maxGallons) {
            this.currentGallons = maxGallons;
            throw new GlassOverflownException("glass capacity exceeded; " + "you have split " + (waterLevel - this.maxGallons) + " gallons.");
        } else {
            this.currentGallons = waterLevel;
        }
    }
}