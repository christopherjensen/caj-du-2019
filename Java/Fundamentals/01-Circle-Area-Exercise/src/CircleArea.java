public class CircleArea {

    public static void main(String[] args) {

        // declare variable area that calls the method with an argument
        double area = calculateCircleArea(15.5);

        // prints out the result of the method
        System.out.println(area);
    }

    // create the method that does the actual computation
    public static double calculateCircleArea(double a) {

        // calculate the formula using the variable a
        return Math.pow(a, 2) * Math.PI;
    }
}
