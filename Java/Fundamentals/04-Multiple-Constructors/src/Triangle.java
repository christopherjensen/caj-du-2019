public class Triangle {

    // declares three variable that can be put on the same line
    // due to being the same type
    float sideA, sideB, sideC;

    // creates a constructor that assigns the values of a, b, c to the variables
    // to be called by the method for calculation
    public Triangle(float a, float b, float c) {
        this.sideA = a;
        this.sideB = b;
        this.sideC = c;
    }

     public Triangle(float sideLength){
        this(sideLength, sideLength, sideLength);
    }


    public float calculatePerimeter() {
        return sideA + sideB + sideC;
    }






}
