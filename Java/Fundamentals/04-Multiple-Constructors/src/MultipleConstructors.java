public class MultipleConstructors {

    public static void main(String[] args) {

        // creates a triangle with the assigned values of sides
        Triangle scaleneTriangle = new Triangle(10, 9, 7.5f);

        // assigns the results of the calculatePerimeter method to the variable
        float scaleneTrianglePerimeter = scaleneTriangle.calculatePerimeter();
        System.out.println("Scalene triangle perimeter = " + scaleneTrianglePerimeter);
        // Prints �Scalene triangle perimeter = 26.5�

        // creates a triable with the assigned vaules of sides
        Triangle equilateralTriangle = new Triangle(10);

        // assigns the results of the calculatePerimeter method to the variable
        float equilateralTrianglePerimeter = equilateralTriangle.calculatePerimeter();
        System.out.println("Equilateral triangle perimeter = " + equilateralTrianglePerimeter);
//        // Prints �Equilateral triangle perimeter = 30.0�

    }

}
