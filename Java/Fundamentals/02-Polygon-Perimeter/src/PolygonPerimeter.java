public class PolygonPerimeter {

    public static void main(String[] args) {

        // create an array that houses the sides of the polygon
        float[] polygonSideLengths = {7.5f, 10.4f, 3.7f, 16f, 20f};// A pentagon

        // assign the result of the method to the variable perimiter
        float perimiter = calculatePerimeter(polygonSideLengths);
        System.out.println(perimiter);
    }

    // method that calculates the area using the values assigned in the array
    public static float calculatePerimeter(float[] length) {
        float sum = 0;
        for (float i : length)
            sum += i;
        return sum;
    }
}
