public class Manager extends Employee {

    public Manager(int empId) {
        super(empId);
    }

    @Override
    public boolean hasAdministratorRights() {
        return true;
    }
}
