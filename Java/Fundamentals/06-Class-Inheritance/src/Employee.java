import java.util.Objects;

public class Employee {

    private Integer idNumber;

    public int getIdNumber() {
        return idNumber;
    }

    Employee(int empId) {
        idNumber = empId;
    }

    public boolean hasAdministratorRights() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(getIdNumber(), employee.getIdNumber());
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash(getIdNumber());
//    }

}

