public class UserClass {

    public static void main(String[] args) {

        // creates new User by calling the User class
        User sampleUser = new User();

        // assigns "Henry" to variable name
        sampleUser.setName("Henry");

        // assigns 1336 to variable score
        sampleUser.setScore(1336);

        // calls the method to increase the variable score by 1
        sampleUser.increaseScoreByOne();

        // prints "User Henry has a socre of: 1337
        System.out.println("User " + sampleUser.getName() + " has a score of: " + sampleUser.getScore()); // Prints ‘User Henry has a score of: 1337’

    }

}


