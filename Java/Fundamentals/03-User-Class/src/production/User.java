public class User {

    // declare variables that will hold values for User
    private String name;
    private int score;

    // method that sends variable name when called
    public String getName() {
        return name;
    }

    // method that assigns value to variable name
    public void setName(String name) {
        this.name = name;
    }

    // method that sends variable score when called
    public int getScore() {
        return score;
    }

    // method that assigns value to variable score
    public void setScore(int score) {
        this.score = score;
    }

    // method that increases score variable by 1
    public void increaseScoreByOne() {
        score++;
    }

}