public class Main {

    public static void main(String[] args) {
        ShoppingCart shoppingCart = new ShoppingCart();

        Item item1 = new Item(10.5f);
        Item item2 = new Item(20.75f);
        Item item3 = new Item(9.99f);
        Item item4 = new Item(14f);
        Item item5 = new Item(100f);
        Item item6 = new Item(55.5f);
        Item item7 = new Item(33.5f);
        Item item8 = new Item(25.0f);
        Item item9 = new Item(25.0f);

        // Add item1 individually
        shoppingCart.addItems(item1);
        // Add the remaining items (item2, item3, item4, item5 and item6) in a single operation
        shoppingCart.addItems(item2, item3, item4, item5, item6);
        // Add another item of price 33.5
        shoppingCart.addItems(item7);
        // Add two items in a single operation, with a combined price of
        shoppingCart.addItems(item8, item9);

        System.out.println(shoppingCart.getNumberOfItems() + " items were added,"
                + " with a total price of $" + shoppingCart.getTotalPrice());
    }

}
