public class ShoppingCart {

    private float totalPrice;
    private int numberOfItems;

    public float getTotalPrice() {
        return totalPrice;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void addItems(float price, int numberOfItems) {
        this.totalPrice += price;
        this.numberOfItems += numberOfItems;
    }

    public void addItems(float price) {
        addItems(price, 1);
//        this.totalPrice += price;
//        numberOfItems++;
    }

    public void addItems(Item item) {
        this.totalPrice += item.getPrice();
        this.numberOfItems++;
    }

    public void addItems(Item... list) {
        for(Item item: list) {
            this.addItems(item);
        }
    }

}
