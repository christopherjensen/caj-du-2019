void function exercise(ksData, Chart) {
    const dataTable = document.getElementById('dataset');
    const cobraBubblesCtx = document.getElementById('CobraBubbles').getContext('2d');

    function renderRow(rowOfData) {
        return rowOfData.reduce((row, column) => {
            const td = document.createElement('td');
            td.appendChild(document.createTextNode(column));
            row.appendChild(td);
            return row
        }, document.createElement('tr'));
    }

    function getViewData(project) {
        const {
            name,
            main_category: category,
            deadline,
            launched,
            state: result,
            country,
            usd_pledged_real: pledged,
            usd_goal_real: goal,
        } = project;

        return [name, category, deadline, goal, launched, pledged, result, country];
    }

    function appendRows(tbody, row) {
        tbody.appendChild(row);
        return tbody
    }

    function renderTable(projects, tbody) {
        projects.map(getViewData)
            .map(renderRow)
            .reduce(appendRows, tbody);
    }

    function getDataset(projects) {
        return Object.entries(projects
            .reduce((dict, { main_category: category }) => {
                if (dict[category]) {
                    dict[category] += 1;
                } else {
                    dict[category] = 1;
                }

                return dict;
            }, {}))
            .map(([label, value]) => ({ label, value }));
    }

    function renderBarChart(ctx, data, options = {}) {
        console.log(data);
        new Chart(ctx, {
            type: 'bar',
            labels: data.map(({ label }) => label),
            data: {
                datasets: [{
                    label: 'Categories',
                    data: data.map(({ value }) => value),
                }]
            },
            options
        });
    }

    renderTable(ksData, dataTable.getElementsByTagName('tbody')[0]);
    renderBarChart(cobraBubblesCtx, getDataset(ksData), {
        responsive: true,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Categories'
        }
    });
}(window.ksData, window.Chart);