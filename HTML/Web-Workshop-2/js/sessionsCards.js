var sessions_1 = document.getElementsByClassName("card-columns")[0];

//Normal File
//const url3 = "https://api.myjson.com/bins/6sia0";


// A ton more objects in the JSON file
const url3 = "https://api.myjson.com/bins/170428"

const request3 = new XMLHttpRequest();
request3.open('GET', url3, true);

request3.onload = function () {
    if (request3.status === 200) {
        const data = JSON.parse(request3.responseText);
        renderComponents(data);

    } else {

    }
}

function renderComponents(data) {
    for (var i = 0; i < data.length; i++) {

        var div = document.createElement("div");
        var div_header = document.createElement("div");
        var div_body = document.createElement("div");
        var h5 = document.createElement("h5");
        var p = document.createElement("p");
        var button = document.createElement("a");
        var div_footer = document.createElement("footer");
        
        var img = document.createElement("img");

        // img.src = "https://cdn2.thecatapi.com/images/6ob.jpg";
        img.src = "https://picsum.photos/300/200/?images=" + randomPic();
        img.alt = "cats";
        img.style = "width: 100%";
        div.className = "card";
        // div.style = "width: 18rem;"
        h5.className = "card-title";
        // div.style = "box-shadow: 3px 6px 6px #888888";
        //div.style = "max-width: 18rem;";
        div_header.className = "card-header card-img-top";
        div_body.className = "card-body";
        button.className = "btn btn-primary d-block"
        button.href = "file:///C:/Users/caj1213/Documents/caj-du-2019/HTML/Web-Workshop-2/start/registration/checkout.html";
        div_footer.className = "card-footer";

        // small.className = "text-muted";

        div_header.appendChild(img);
        div.appendChild(div_header);
        div.appendChild(div_body);
        
        // small.innerHTML = "Item Choice";
        button.innerHTML = "Check Out";
        h5.innerHTML = data[i].title;
        div_body.appendChild(h5);
        p.innerHTML = data[i].starttime + "<br> " + data[i].duration + "<br>" + data[i].cost;
        p.className = "card-text text-center";
        div_body.appendChild(p);
        div_body.appendChild(button);
        div_footer.appendChild(button);
        div.appendChild(div_footer);

        //row.appendChild(div);
        sessions_1.appendChild(div);
    }
}

function randomPic() {
  var pic = Math.floor((Math.random() * 800) + 1);
  return pic;
}


request3.onerror = function () {
    console.error('An error occurred fetching the JSON from ' + url3);
};

request3.send();