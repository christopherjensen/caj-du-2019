let dropdown_1 = document.getElementById('state-dropdown');
dropdown_1.length = 0;

let defaultOption_1 = document.createElement('option');
defaultOption_1.text = 'Choose State';

dropdown_1.add(defaultOption_1);
dropdown_1.selectedIndex = 0;

const url_1 = 'https://api.jsonbin.io/b/5c521bbfe9e7c1183904d0ce';

const request_1 = new XMLHttpRequest();
request_1.open('GET', url_1, true);

request_1.onload = function() {
  if (request_1.status === 200) {
    const data = JSON.parse(request_1.responseText);
    let option;
    for (let i = 0; i < data.length; i++) {
      option = document.createElement('option');
      option.text = data[i].name;
      option.value = data[i].abbreviation;
      dropdown_1.add(option);
    }
   } else {
    // Reached the server, but it returned an error
  }   
}

request_1.onerror = function() {
  console.error('An error occurred fetching the JSON from ' + url);
};

request_1.send();