/**
 * Function that finds and returns the biggest positive divisor of the given positive integer
 */
function findBiggestDivisor (positiveInteger) {
  console.log("Finding the biggest divisor of: " + positiveInteger);
  // Your code goes here
  let divisors = [];

  for (let i = 1; i < positiveInteger; i++) {
    if (positiveInteger % i === 0) {
      let a = divisors.push(i);
    } 
  }

  if (Math.max(...divisors) === 1) {
    return 'NaN';
  } else {
    return Math.max(...divisors);
  }
}


// Test it with different values
console.log(findBiggestDivisor(7));
console.log(findBiggestDivisor(124));
console.log(findBiggestDivisor(2097));
console.log(findBiggestDivisor(2));
console.log(findBiggestDivisor(4));
console.log(findBiggestDivisor(11));
console.log(findBiggestDivisor(125));