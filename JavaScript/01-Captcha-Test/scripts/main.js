// Your code goes here
// Hint: use "prompt(promptMessage)" to ask for user input - it will return the value input by the user
// And "confirm(confirmMessage)" to display a confirmation dialog - it will return true if the user accepts and false if the user cancels
// Both functions stop execution and wait till the user has entered a value or made a selection


// function getRandomNumbers(min, max) {
//   min = Math.ceil(min);
//   max = Math.floor(max);
//   return Math.floor(Math.random() * (max - min) + min);
// }

// numberOne = getRandomNumbers(0, 100);
// numberTwo = getRandomNumbers(0, 100);
// sum = numberOne + numberTwo;

// console.log(numberOne, numberTwo);

// userInput = () => {
//   let number = prompt(`What is ${numberOne} + ${numberTwo}`);

//   if (number != null) {
//     confirm(`Confirm: Are you sure that ${numberOne} + ${numberTwo} = ${sum}?`)
//   } else if ( number === sum ) {
//     console.log('Human user identified');
//   } else if (number != sum) {
//     prompt(`That's not correct. Try again. How much is ${numberOne} + ${numberTwo}?`)
//   } else {
//     console.log('Human user identified');
//   }
// };

// userInput();

function randomInteger (positiveInteger) { 
  return Math.round(Math.random() * 99);
}

var firstInteger = randomInteger();
var secondInteger = randomInteger();

var promptMessage = `How much is ${firstInteger} + ${secondInteger}?`;
var expectedResult = firstInteger + secondInteger;

var inputNumber = null;
var isFirstTry = true;
// Repeat the prompt till the user enters the expected result
// Exact inequality (!== vs. !=) is not used in this comparison, since the expected result is a number, but the user input is a String
while (inputNumber != expectedResult) {

  // If the user doesn't confirm the input, he will try again
  var userConfirmed = false;
  while(!userConfirmed) {
      var inputNumber = prompt(promptMessage);
      userConfirmed = confirm(`Are you sure ${firstInteger} + ${secondInteger} = ${inputNumber}?`);
  }

  // If it is the first try, update the prompt message for any other try
  if(isFirstTry) {
      isFirstTry = false;
      promptMessage = `That's not correct. ${promptMessage}`;
  }
}

console.log("Human user identified");