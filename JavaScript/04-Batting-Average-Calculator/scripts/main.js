/**
 * Function that calculates the batting average
 * @param strikes Array with Boolean values. True represents a strike, while false is not a strike (and hence, assumed a hit.)
 * @returns Number with the batting average of the player
 */
calculateBattingAverage = (strikes) => {
  // Your code goes here
  let strikeCounter = 0;

    for( i = 0; i < strikes.length; i++ ) {
      if (strikes[i] == false)
        strikeCounter++;  
    }
    return strikeCounter / strikes.length;
  }


// Test it with these values
var exampleStrikes = [true, true, false, true, false, true, true, true, true, false, false, true, true, true, true];
console.log(calculateBattingAverage(exampleStrikes)); // returns 0.26666666666666666

